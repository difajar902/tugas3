import React from 'react';
import {View, ScrollView, Image, Text, StyleSheet} from 'react-native';
import Content2 from '../assets/irsoekarno.png';
import Content22 from '../assets/sangsingapodium.png';
import Sukasuka from '../assets/love2.jpg';
import Komentar from '../assets/comment.jpg';
import Kirim from '../assets/img.jpg';
import Bmk from '../assets/bookmark1.jpg';
import Di from '../assets/dini.png';
import Emoji from '../assets/emojilove.jpg';
import Emoji1 from '../assets/emoji.jpg';

const Body2 = () => {
    return (
        <View>
            <ScrollView>
                <View>
                    <View style={styles.cont}>
                        <View style={styles.icon}>
                            <Image source={Content2} style={styles.img}/>
                        </View>
                    <View>
                        <Text style={styles.text1}>ir.soekarno</Text>
                    </View>
                        <Text style={styles.text}>...</Text>
                    </View>
                    <View>
                        <Image source={Content22} style={styles.img2}/>
                    </View>
                    <View style={styles.cont2}>
                        <View style={styles.icon2}>
                            <Image source={Sukasuka} style={styles.suka}/>
                        </View>
                        <View style={styles.icon3}>
                            <Image source={Komentar} style={styles.comment}/>
                        </View>
                        <View style={styles.icon4}>
                            <Image source={Kirim} style={styles.send}/>
                        </View>
                        <View style={styles.icon5}>
                            <Image source={Bmk} style={styles.bookmark}/>
                        </View>
                    </View>
                    <Text style={styles.like}>11.314 suka</Text>
                    <View style={styles.cont3}>
                        <Text style={styles.ket}>ir.soekarno</Text>
                        <Text style={{marginLeft: 5}}>Sang singa podium RI (ir.soekarno)</Text>
                    </View>
                    <View style={styles.cont4}>
                        <Text style={{color: 'navy'}}>#presidenri #pejuangri</Text>
                        <Text style={{color: 'grey'}}>Lihat semua 213 komentar</Text>
                    </View>
                    <View style={styles.cont5}>
                        <View style={styles.icon6}>
                            <Image source={Di} style={styles.dini}/>
                        </View>
                        <Text style={{marginTop: 12, marginLeft: 10, color: 'grey'}}>Tambahkan komentar...</Text>
                        <View>
                            <Image source={Emoji} style={styles.emoji1}/>
                        </View>
                        <View>
                            <Image source={Emoji1} style={styles.emoji2}/>
                        </View>
                        <View style={styles.icon7}>
                            <Text style={styles.text3}>+</Text>
                        </View>
                    </View>
                    <Text style={{marginLeft: 15, marginTop: 1, color: 'grey', fontSize: 10}}>3 hari yang lalu</Text>
                </View>
            </ScrollView>
        </View>
    );
};
const styles = StyleSheet.create ({
    cont: {
        flexDirection: 'row'
    },
    icon: {
        borderWidth: 2,
        borderColor: 'white',
        width: 45,
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 65,
        marginLeft: 5,
        marginTop: 10,
    },
    img: {
        borderRadius: 100,
        width: 38,
        height: 38
    },
    text1: {
        marginBottom: 0, 
        marginTop: 20, 
        marginLeft: 5, 
        fontSize: 15, 
        fontWeight: 'bold'
    },
    text: {
        fontSize: 19,
        textAlign: 'right',
        marginLeft: 190,
        marginTop: 12
    },
    img2: {
        width: 400,
        height: 350,
        marginTop: 5,
    },
    cont2: {
        flexDirection: 'row'
    },
    icon2: {
        borderWidth: 1,
        borderColor: 'white',
        width: 50,
        height: 50,
        borderRadius: 80,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 0,
        marginLeft: 3
    },
    suka: {
        borderRadius: 50,
        width: 49,
        height: 49,
        position: 'absolute'
    },
    icon3: {
        borderWidth: 1,
        borderColor: 'white',
        width: 50,
        height: 50,
        borderRadius: 80,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 0,
        marginLeft: 0
    },
    comment: {
        borderRadius: 100,
        width: 36,
        height: 36,
        position: 'absolute',
        marginTop: 5
    },
    icon4: {
        borderWidth: 1,
        borderColor: 'white',
        width: 50,
        height: 50,
        borderRadius: 80,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 0,
        marginLeft: 0
    },
    send: {
        borderRadius: 100,
        width: 46.5,
        height: 46.5,
        position: 'absolute',
        marginTop: 2,
    },
    icon5: {
        borderWidth: 1,
        borderColor: 'white',
        width: 50,
        height: 50,
        borderRadius: 80,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 0,
        marginLeft: 155
    },
    bookmark: {
        borderRadius: 100,
        width: 50,
        height: 50,
        position: 'absolute',
        marginTop: 2
    },
    like: {
        fontWeight: 'bold',
        fontFamily: 'arial',
        marginLeft: 15,
        marginTop: 0
    },
    cont3: {
        flexDirection: 'row',
    },
    ket: {
        fontSize: 14,
        fontWeight: 'bold',
        fontFamily: 'arial',
        marginLeft: 17,
        marginTop: 0,
    },
    cont4: {
        marginLeft: 17,
        marginTop: 2
    },
    cont5: {
        flexDirection: 'row',
        marginLeft: 7
    },
    icon6: {
        borderWidth: 1,
        borderColor: 'white',
        width: 35,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 65,
        marginLeft: 5,
        marginTop: 5,
    },
    dini: {
        borderRadius: 100,
        width: 30,
        height: 30,
        position: 'absolute'
    },
    emoji1: {
        borderRadius: 100,
        width: 25,
        height: 25,
        marginTop: 10,
        marginLeft: 60
    },
    emoji2: {
        borderRadius: 100,
        width: 30,
        height: 30,
        marginTop: 3,
    },
    icon7: {
        borderWidth: 1,
        borderColor: 'grey',
        width: 15,
        height: 15,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100,
        marginLeft: 5,
        marginTop: 13,
    },
    text3: {
        color: 'grey',
        width: 10,
        height: 22,
        position: 'absolute',
    }

})
export default Body2;