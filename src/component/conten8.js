import React from 'react';
import {View, ScrollView, Image, Text, StyleSheet} from 'react-native';
import Content8 from '../assets/violin_keyboard.png';
import Conten88 from '../assets/arlan.jpg';
import Suka from '../assets/love2.jpg';
import Comment from '../assets/comment.jpg';
import Send from '../assets/img.jpg';
import Bookmark from '../assets/bookmark1.jpg';
import Dinifajar from '../assets/dini.png';
import Emoji1 from '../assets/emojilove.jpg';
import Emoji2 from '../assets/emoji.jpg';

const Body8 = () => {
    return (
        <View>
            <ScrollView>
                <View style={styles.container}>
                    <View style={styles.icon}>
                        <Image source={Content8} style={styles.logo}/>
                    </View>
                <View style={styles.container2}>
                    <Text style={styles.text}>festivalmusic</Text>
                    <Text style={{marginLeft: 5, marginTop: 0, fontSize: 13}}>Monumen Arek Lancor</Text>
                </View>
                    <Text style={styles.text2}>...</Text>
                </View>
                <View>
                    <Image source={Conten88} style={styles.img}/>
                </View>
                <View style={styles.container3}>
                    <View style={styles.icon2}>
                        <Image source={Suka} style={styles.suka}/>
                    </View>
                    <View style={styles.icon3}>
                        <Image source={Comment} style={styles.comment}/>
                    </View>
                    <View style={styles.icon4}>
                        <Image source={Send} style={styles.send}/>
                    </View>
                    <View style={styles.icon5}>
                        <Image source={Bookmark} style={styles.bookmark}/>
                    </View>
                </View>
                <Text style={styles.like}>6.117 suka</Text>
                <View style={styles.container4}>
                    <Text style={styles.ket}>festivalmusic</Text>
                    <Text style={{marginLeft: 5}}>Hari jadi PAMEKASAN yang ke-490</Text>
                </View>
                <View style={styles.container5}>
                    <Text style={{color: 'navy'}}>#pamekasan #pamekasanhits</Text>
                    <Text style={{color: 'navy'}}>#madura #festivalmusikmadura</Text>
                    <Text style={{color: 'grey'}}>Lihat semua 845 komentar</Text>
                </View>
                <View style={styles.container6}>
                    <View style={styles.icon6}>
                        <Image source={Dinifajar} style={styles.dini}/>
                    </View>
                    <Text style={{marginTop: 12, marginLeft: 10, color: 'grey'}}>Tambahkan komentar...</Text>
                    <View>
                        <Image source={Emoji1} style={styles.emoji1}/>
                    </View>
                    <View>
                        <Image source={Emoji2} style={styles.emoji2}/>
                    </View>
                    <View style={styles.icon7}>
                        <Text style={styles.text3}>+</Text>
                    </View>
                </View>
                <Text style={{marginLeft: 15, marginTop: 1, color: 'grey', fontSize: 10}}>6 bulan yang lalu</Text>
                <View />
            </ScrollView>
        </View>
    );
};
const styles = StyleSheet.create ({
    container: {
        flexDirection: 'row',
    },
    icon: {
        borderWidth: 2,
        borderColor: 'red',
        width: 45,
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 65,
        marginLeft: 5,
        marginTop: 10,
    },
    logo: {
        borderRadius: 100,
        width: 38,
        height: 38
    },
    img: {
        width: 370,
        height: 350,
        marginTop: 10,
        marginRight: 0,
        margin: 0
    },
    text: {
        marginBottom: 0,
        marginTop: 12,
        marginLeft: 5,
        fontSize: 15,
        fontWeight: 'bold'
    },
    text2: {
        fontSize: 19,
        textAlign: 'right',
        marginLeft: 130,
        marginTop: 15
    },
    container3: {
        flexDirection: 'row'
    },
    icon2: {
        borderWidth: 1,
        borderColor: 'white',
        width: 50,
        height: 50,
        borderRadius: 80,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 0,
        marginLeft: 3

    },
    suka: {
        borderRadius: 50,
        width: 49,
        height: 49,
        position: 'absolute'
    },
    icon3: {
        borderWidth: 1,
        borderColor: 'white',
        width: 50,
        height: 50,
        borderRadius: 80,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 0,
        marginLeft: 0
    },
    comment: {
        borderRadius: 100,
        width: 36,
        height: 36,
        position: 'absolute',
        marginTop: 5
    },
    icon4: {
        borderWidth: 1,
        borderColor: 'white',
        width: 50,
        height: 50,
        borderRadius: 80,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 0,
        marginLeft: 0
    },
    send: {
        borderRadius: 100,
        width: 46.5,
        height: 46.5,
        position: 'absolute',
        marginTop: 2,
    },
    icon5: {
        borderWidth: 1,
        borderColor: 'white',
        width: 50,
        height: 50,
        borderRadius: 80,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 0,
        marginLeft: 155
    },
    bookmark: {
        borderRadius: 100,
        width: 50,
        height: 50,
        position: 'absolute',
        marginTop: 2,
    },
    like: {
        fontWeight: 'bold',
        fontFamily: 'arial',
        marginLeft: 15,
        marginTop: 0,
    },
    container4: {
        flexDirection: 'row'
    },
    ket: {
        fontSize: 14,
        fontWeight: 'bold',
        fontFamily: 'arial',
        marginLeft: 15,
        marginTop: 0,
    },
    container5: {
        marginLeft: 15,
        marginTop: 2
    },
    container6: {
        flexDirection: 'row',
        marginLeft: 7
    },
    icon6: {
        borderWidth: 1,
        borderColor: 'white',
        width: 35,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 65,
        marginLeft: 5,
        marginTop: 5,
    },
    dini: {
        borderRadius: 100,
        width: 30,
        height: 30,
        position: 'absolute'
    },
    emoji1: {
        borderRadius: 100,
        width: 25,
        height: 25,
        marginTop: 10,
        marginLeft: 60
    },
    emoji2: {
        borderRadius: 100,
        width: 30,
        height: 30,
        marginTop: 3,
    },
    icon7: {
        borderWidth: 1,
        borderColor: 'grey',
        width: 15,
        height: 15,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100,
        marginLeft: 5,
        marginTop: 13,
    },
    text3: {
        color: 'grey',
        width: 10,
        height: 22,
        position: 'absolute',
    }
})
export default Body8;