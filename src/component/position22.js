import React from 'react';
import {Text, Image, ScrollView, View, StyleSheet} from 'react-native';
import Position22 from '../assets/violin_keyboard2.png';

const Friend2 = () => {
    return (
        <View>
            <ScrollView>
                <View style={styles.first}>
                    <View style={styles.icon}>
                        <Image source={Position22} style={styles.image}/>
                    </View>
                    <Text style={styles.text3}>genremu..</Text>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create ({
    first: {
        marginTop : 0,
        marginLeft: 0
    },
    icon: {
        borderWidth: 2,
        borderColor: 'red',
        width: 68,
        height: 68,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 65,
        marginLeft: 5
    },
    image: {
        width: 60,
        height: 60,
        borderRadius: 100
    },
    text3: {
        fontSize: 13,
        margin: 5,
        marginLeft: 10,
        marginTop: 0
    }
})
export default Friend2;