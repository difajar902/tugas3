import React from 'react';
import {Text, View, ScrollView, Image, StyleSheet} from 'react-native';
import Position21 from '../assets/violin_keyboard.png';

const Friend = () => {
    return (
        <View>
            <ScrollView>
                <View style={styles.first}>
                    <View style={styles.icon}>
                        <Image source={Position21} style={styles.img}/>
                    </View>
                    <Text style={styles.text2}>festivalm..</Text>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create ({
    first: {
        marginTop: 0,
        marginLeft: 0,
    },
    icon: {
        borderWidth: 2,
        borderColor: 'red',
        width: 68,
        height: 68,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 65,
        marginLeft: 5
    },
    img: {
        width: 60,
        height: 60,
        borderRadius: 100
    },
    text2: {
        fontSize: 13,
        margin: 5,
        marginLeft: 9,
        marginTop: 0
    }  
})
export default Friend;