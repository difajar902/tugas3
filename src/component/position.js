import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import Message from '../assets/message1.jpg';
 
const Position = () => {
    return (
        <View style={styles.first}>
            <View style={styles.icon}>
                <Image source={Message} style={styles.image}/>
            </View>
            <Text style={styles.notif}>5</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    first: {
        marginTop: 5,
    },
    icon: {
        borderWidth: 1,
        borderColor: 'white',
        width: 50,
        height: 50,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        width: 51,
        height: 51,
    },
    notif: {
        fontSize: 12,
        color: 'white',
        backgroundColor: 'red',
        padding : 2,
        width: 20,
        height: 20,
        borderRadius: 80,
        position: 'absolute',
        textAlign: 'center',
        right: 0

    }
})
export default Position;